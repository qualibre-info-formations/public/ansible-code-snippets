ansible -i hosts -m user -a "name=ansible-user \
    password={{ 'passforce' | password_hash('sha512', 'secretsalt') }}" --user root all
