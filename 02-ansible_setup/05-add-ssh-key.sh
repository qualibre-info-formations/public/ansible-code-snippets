ansible -i hosts -m authorized_key -a 'user=ansible-user state=present \
    key="{{ lookup("file", "/home/user/.ssh/id_rsa.pub") }}"' --user ansible-user \
    --ask-pass all