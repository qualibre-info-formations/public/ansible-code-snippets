ansible -i hosts -m lineinfile -a 'path=/etc/sudoers \
    line="%sudo        ALL=(ALL)       NOPASSWD: ALL" regexp=".*%sudo.*"' \
    --user ansible-user --become --ask-become-pass all