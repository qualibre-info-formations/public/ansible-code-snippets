# Exercices

## Exercice de création de rôle

Les fichiers devant être placés sur le serveur web sont ici : https://gitlab.com/qualibre-info-formations/public/misc-sources/-/tree/master/ansible-files

## Solution

https://gitlab.com/qualibre-info-formations/public/mediawiki-deploy.git

# Les modules Ansible

Créer un module Ansible : https://docs.ansible.com/ansible/latest/dev_guide/developing_modules_general.html#creating-a-module

## Mettre en place l'environnement de développeur Ansible
```bash
sudo yum check-update
sudo yum update
sudo yum group install "Development Tools"
sudo yum install python3-devel openssl-devel libffi libffi-devel
```

```bash
git clone https://github.com/ansible/ansible.git
cd ansible
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
. hacking/env-setup
```